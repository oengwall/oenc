#include <iostream>
#include <thread>
#include <atomic>
#include "client.hpp"

struct options {
    options(): server("localhost"), port(1480) {}

    std::string server;
    unsigned short port;
};

options
parse_options(int argc, char *argv[]) {
    // consider cxxopts
    options opt;
    if(argc > 1)
        opt.server = argv[1];
    if(argc > 2)
        opt.port = std::atoi(argv[2]);
    return opt;
}

int main(int argc, char *argv[]) {
    auto opts = parse_options(argc, argv);
    asio::io_service io;
    client c(io, opts.server, opts.port);
    try {
        c.run();
        io.run();
    } catch (std::exception const& err) {
        std::cerr << "err: " << err.what() << std::endl;
    }
    if(!io.stopped())
        io.stop();
    c.stop();
    return 0;
}
