#pragma once

#include <memory>
#include <atomic>
#include <array>
#include <string>
#include <asio.hpp>


class receiver: public std::enable_shared_from_this<receiver> {
public:
    typedef std::shared_ptr<receiver> pointer;

    static auto create(asio::ip::tcp::socket& sock, std::ostream& out);
    void receive_data();

private:
    receiver(asio::ip::tcp::socket& sock, std::ostream& out);
    void data_received(asio::error_code const& err, std::size_t len);

    asio::ip::tcp::socket& sock_;
    std::ostream& out_;
    std::array<char, 1024> data_;
};


class sender: public std::enable_shared_from_this<sender> {
public:
    typedef std::shared_ptr<sender> pointer;

    static auto create(asio::io_service& io);
    auto& socket() {return sock_;}
    bool connect(std::string const& server, unsigned short port);
    void send_line(std::string const& message);

private:
    sender(asio::io_service& io): 
        sock_(io) {}
    
    asio::ip::tcp::socket sock_;
};


class client {
public:
    client(asio::io_service& io, std::string const& server, unsigned short port);
    void run();
    void stop();
private:
    void read_and_send_line();
    
    asio::io_service& io_;
    std::string server_; 
    unsigned short port_;
    std::atomic_bool finished_;
    std::shared_ptr<sender> sender_;
    std::shared_ptr<receiver> receiver_;
    std::thread stdin_reader_;
};
