#include <iostream>
#include <functional>
#include "client.hpp"

auto
sender::create(asio::io_service& io) {
    return pointer(new sender(io));
}

bool 
sender::connect(std::string const& server, unsigned short port) {
    asio::ip::tcp::resolver resolver(sock_.get_io_service());
    asio::ip::tcp::resolver::query query(server, std::to_string(port));
    auto it = resolver.resolve(query);
    asio::connect(sock_, it);
    return true;
}

void
sender::send_line(std::string const& message) {
    auto data = std::make_unique<std::string>(message);
    //std::cerr << "SENDING: " << *data << std::endl;
    data->push_back('\n');
    auto buffer = asio::buffer(data->c_str(), data->size());
    asio::async_write(sock_, buffer,
        [data=std::move(data)](asio::error_code const& err, std::size_t len){
            if(err) {
                std::cerr << "ERROR: " << err.message() << std::endl;
            }
        }
    );
}

receiver::receiver(asio::ip::tcp::socket& sock, std::ostream& out):
    sock_(sock), out_(out) {
 }

auto
receiver::create(asio::ip::tcp::socket& sock, std::ostream& out) {
    return pointer(new receiver(sock, out));
}

void
receiver::receive_data() {
    auto buffer = asio::buffer(data_);
    pointer that = shared_from_this();
    asio::async_read(sock_, 
                     buffer,
                     asio::transfer_at_least(1),
                     [that](asio::error_code const& err, std::size_t len){
        if(!err) {
            std::string d{that->data_.begin(), that->data_.begin() + len};
            that->out_ << d; 
            that->out_.flush();
            that->receive_data();
        } else {
            std::cerr << "network error: " << err.message() << std::endl;
        }
    });
}

void 
receiver::data_received(asio::error_code const& err, std::size_t len) {

}


client::client(asio::io_service& io, std::string const& server, unsigned short port)
    : io_(io), server_(server), port_(port)
{
}



void
client::read_and_send_line() {
    auto messagep = std::make_unique<std::string>();
    std::getline(std::cin, *messagep);
    auto sender = sender_;
    asio::post([sender, messagep=std::move(messagep)]{
        sender->send_line(*messagep);
    });
}

void 
client::run() {
    finished_ = false;
    sender_ = sender::create(io_);
    sender_->connect(server_, port_);
    receiver_ = receiver::create(sender_->socket(), std::cout);
    receiver_->receive_data();
    stdin_reader_ = std::thread([this](){
        while (!this->finished_) {
            read_and_send_line();
        }
    });
}

void
client::stop() {
    finished_ = true;
    if(stdin_reader_.joinable())
        stdin_reader_.join();
}